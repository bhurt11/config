set expandtab
set shiftwidth=4
set tabstop=4
set hidden
set signcolumn=yes:2
set relativenumber
set number
set termguicolors
set undofile
set spell
set title
set ignorecase
set smartcase
set wildmode=longest:full,full
set nowrap
set list
set listchars=tab:▸\ ,trail:·
set mouse=a
set scrolloff=8
set sidescrolloff=8
set nojoinspaces
set splitright
set clipboard=unnamedplus
set confirm
set exrc
set backup
set backupdir=~/.local/share/nvim/backup//
set updatetime=300 " Reduce time for highlighting other references
set redrawtime=10000 " Allow more time for loading syntax on large files
set sessionoptions+=globals
set omnifunc=syntaxcomplete#Complete
set spell


filetype plugin on

"--------------------------------------------------------------------------
" Key maps
"--------------------------------------------------------------------------

let mapleader = "\<space>"

"nnoremap <leader>/ :%s/\v/gc<Left><Left><Left>
"nnoremap gg :term <CR> emacsclient -nw -c "(magit-status)" <CR>

" define line highlight color
highlight LineHighlight ctermbg=darkgray guibg=darkgray
" highlight the current line
nnoremap  <leader>k :call matchadd('LineHighlight', '\%'.line('.').'l')<CR>
" clear all the highlighted lines
nnoremap <leader>x  :call clearmatches()<CR>

nmap <leader>ve :edit ~/.config/nvim/init.vim<cr>
nmap <leader>vc :edit ~/.config/nvim/coc-settings.json<cr>
nmap <leader>vr :source ~/.config/nvim/init.vim<cr>
nmap <leader>sz :source ~/.vimrc<cr>
nmap gm :LivedownToggle<CR>

" Allow gf to open non-existent files
map gf :edit <cfile><cr>

"------------------------
"remap for yank to end of line ;-)
map Y y$

" ------------------
" copy and paste with "+y and "+p

vmap <C-c> "+yi
vmap <C-x> "+c
vmap <C-v> c<ESC>"+p
imap <C-v> <ESC>"+pa

"vnoremap <c-c>"+y
"map <c-v>"+P
" ------------------</c-v></c-c>

"cmap w!! :execute ':silent %!sudo tee > /dev/null %' | :edit!
command w!! :execute ':silent %!sudo tee > /dev/null %' | :edit!



"cmap w! :execute ':silent w !sudo tee % > /dev/null' | :edit!
command w! :execute ':silent w !sudo tee % > /dev/null' | :edit!

" Install vim-plug if not found
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

" Run PlugInstall if there are missing plugins
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \| PlugInstall --sync | source $MYVIMRC
\| endif



" Automatically install vim-plug
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
"/.local/share/nvim/site
call plug#begin('~/.local/share/nvim/site/plugins')
"call plug#begin(data_dir . '/plugins')

source ~/.config/nvim/plugins/abolish.vim
source ~/.config/nvim/plugins/arduino.vim
source ~/.config/nvim/plugins/airline.vim
source ~/.config/nvim/plugins/bufferline.vim
source ~/.config/nvim/plugins/coc.vim
  "source ~/.config/nvim/plugins/clearreg1.vim
  "source ~/.config/nvim/plugins/clearreg.vim
  "luafile ~/.config/nvim/lua/setup/plug-colorizer.lua
source ~/.config/nvim/plugins/colorizer.vim
source ~/.config/nvim/plugins/coc-explorer.vim
source ~/.config/nvim/plugins/commentary.vim
source ~/.config/nvim/plugins/vim-airline-themes.vim
source ~/.config/nvim/plugins/context-commentstring.vim
source ~/.config/nvim/plugins/dispatch.vim
source ~/.config/nvim/plugins/dracula.vim
source ~/.config/nvim/plugins/editorconfig.vim
source ~/.config/nvim/plugins/eunuch.vim
source ~/.config/nvim/plugins/exchange.vim
source ~/.config/nvim/plugins/firenvim.vim
source ~/.config/nvim/plugins/floaterm.vim
source ~/.config/nvim/plugins/fugitive.vim
source ~/.config/nvim/plugins/fvim.vim
source ~/.config/nvim/plugins/fzf.vim
source ~/.config/nvim/plugins/gitsigns.nvim.vim
source ~/.config/nvim/plugins/git-worktree.vim
source ~/.config/nvim/plugins/heritage.vim
source ~/.config/nvim/plugins/lastplace.vim
source ~/.config/nvim/plugins/lion.vim
"luafile ~/.config/nvim/lua/setup/lsp-installer.lua
source ~/.config/nvim/plugins/lspkind-nvim.vim
source ~/.config/nvim/plugins/markdown-preview.vim
source ~/.config/nvim/plugins/neogen.vim
source ~/.config/nvim/plugins/nerdtree.vim
source ~/.config/nvim/plugins/nvim-cmp.vim
"source ~/.config/nvim/plugins/nvim-lspconfig.vim
source ~/.config/nvim/plugins/nvim-lsp-installer.vim
source ~/.config/nvim/plugins/nvim-notify.vim
source ~/.config/nvim/plugins/nvim-web-devicon.vim
source ~/.config/nvim/plugins/pasta.vim
source ~/.config/nvim/plugins/peekaboo.vim
source ~/.config/nvim/plugins/phpactor.vim
source ~/.config/nvim/plugins/polyglot.vim
source ~/.config/nvim/plugins/projectionist.vim
source ~/.config/nvim/plugins/quickscope.vim
source ~/.config/nvim/plugins/repeat.vim
source ~/.config/nvim/plugins/rnvimr.vim
source ~/.config/nvim/plugins/rooter.vim
source ~/.config/nvim/plugins/rust.vim
source ~/.config/nvim/plugins/sayonara.vim
source ~/.config/nvim/plugins/smooth-scroll.vim
source ~/.config/nvim/plugins/splitjoin.vim
source ~/.config/nvim/plugins/sudoedit.vim
source ~/.config/nvim/plugins/surround.vim
source ~/.config/nvim/plugins/targets.vim
source ~/.config/nvim/plugins/telescope.nvim.vim
source ~/.config/nvim/plugins/textobj-xmlattr.vim
source ~/.config/nvim/plugins/tmux-navigator.vim
source ~/.config/nvim/plugins/nvim-tree.lua
source ~/.config/nvim/plugins/nvim-treesitter.vim
source ~/.config/nvim/plugins/unimpaired.vim
source ~/.config/nvim/plugins/vim-expand-region.vim 
source ~/.config/nvim/plugins/vim-livedown.vim
source ~/.config/nvim/plugins/vim-markdown.vim
source ~/.config/nvim/plugins/vim-magit.vim
source ~/.config/nvim/plugins/vim-test.vim
source ~/.config/nvim/plugins/vim-textobj-between.vim
source ~/.config/nvim/plugins/vim-textobj-brace.vim
source ~/.config/nvim/plugins/vim-textobj-comment.vim
source ~/.config/nvim/plugins/vim-textobj-continuous-line.vim
source ~/.config/nvim/plugins/vim-textobj-datetime.vim
source ~/.config/nvim/plugins/vim-textobj-diff.vim
source ~/.config/nvim/plugins/vim-textobj-entire.vim
source ~/.config/nvim/plugins/vim-textobj-erb.vim
source ~/.config/nvim/plugins/vim-textobj-fold.vim
source ~/.config/nvim/plugins/vim-textobj-user.vim
source ~/.config/nvim/plugins/visual-multi.vim
source ~/.config/nvim/plugins/visual-star-search.vim
"source ~/.config/nvim/plugins/which-key-folke.vim
source ~/.config/nvim/plugins/which-key.vim
source ~/.config/nvim/plugins/wordwrap.vim


call plug#end()
