 
(("default" . ((user-emacs-directory . "~/.emacs.default")))
 ("spacemacs" . ((user-emacs-directory . "~/emacs/spacemacs")))
 ("tecosaur" . ((user-emacs-directory . "~/.config/doom")))
 ("dotemacs" . ((user-emacs-directory . "~/emacs/dotemacs")))
 ("doom" . ((user-emacs-directory . "~/emacs/doom"))))
