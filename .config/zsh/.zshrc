#!/bin/bash
neofetch

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

typeset -g POWERLEVEL9K_INSTANT_PROMPT=quiet

source ~/powerlevel10k/powerlevel10k.zsh-theme

typeset -U path

# Print a greeting message when shell is started
#echo $USER@$HOST  $(uname -srm) $(lsb_release -rcs)
echo $USER@$(lsb_release -ds) $(lsb_release -rs) $(uname -rs)
export ZDOTDIR=$HOME/.config/zsh
source "$HOME/.config/zsh/zshrc"

#source $HOME/.cargo/env
source /usr/share/nvm/init-nvm.sh
source ~/powerlevel10k/powerlevel10k.zsh-theme
source ~/clone/zsh-z/zsh-z.plugin.zsh

source /home/brandon/.config/broot/launcher/bash/br
