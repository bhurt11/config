--vim.cmd 'syntax on'
--vim.cmd 'filetype plugin indent on'
--vim.g.mapleader = ' '
--print(vim.inspect(vim.g.mapleader))
--vim.g.vimsyn_embed = 'lPr'


vim.o.expandtab = true
vim.o.shiftwidth='4'
vim.o.tabstop='4'
vim.o.hidden = true
vim.o.signcolumn='yes:2'
vim.o.relativenumber = true
vim.o.number = true
vim.o.termguicolors = true
vim.o.undofile = true
vim.o.spell = true
vim.o.title = true
vim.o.ignorecase = true
vim.o.smartcase = true
vim.o.wildmode='longest:full,full'
vim.o.nowrap = true
vim.o.list = true
vim.o.listchars = {
    tab         = '▷⋯ ',
    trail       = '·',
}
vim.o.mouse='a'
vim.o.scrolloff='8'
vim.o.sidescrolloff='8'
vim.o.nojoinspaces = true
vim.o.splitright = true
vim.o.clipboard='unnamedplus'
vim.o.confirm = true
vim.o.exrc = true
vim.o.backup = true
vim.o.backupdir= '~/.local/share/nvim/backup/'
vim.o.updatetime= '300' -- Reduce time for highlighting other references
vim.o.redrawtime= '10000' -- Allow more time for loading syntax on large files
vim.o.sessionoions:append('globals')
vim.o.omnifunc= 'syntaxcomplete'
vim.o.spell = true
vim.o.wildchars = 26 --('<C-z>') substitute for wildchar (<Tab>) in macros


-- remap Y to yank to end of line
vim.api.nvim_set_keymap('n', 'Y', 'y$', { noremap = true })
vim.api.nvim_set_keymap('v', 'Y', 'y$', { noremap = true })

-- delete text without yanking
vim.api.nvim_set_keymap('n', '<leader>d', '"_d', { noremap = true })
vim.api.nvim_set_keymap('v', '<leader>d', '"_d', { noremap = true })

-- turn off search highlighting
vim.api.nvim_set_keymap('n', '<leader>hl', ':nohlsearch<cr>', { noremap = true })

vim.g.mapleader = ' '
--vim.g.maplocalleader = '\\'
-- let mapleader = "\<space>"

--nnoremap <leader>/ :%s/\v/gc<Left><Left><Left>
--nnoremap gg :term <CR> emacsclient -nw -c "(magit-status)" <CR>
--nmap <leader>ve :edit ~/.config/nvim/init.vim<cr>
--nmap <leader>vc :edit ~/.config/nvim/coc-settings.json<cr>
--nmap <leader>vr :source ~/.config/nvim/init.vim<cr>
--nmap <leader>sz :source ~/.vimrc<cr>
--nmap gm :LivedownToggle<CR>

--" Allow gf to open non-existent files
--map gf :edit <cfile><cr>

--"------------------------
--"remap for yank to end of line ;-)
--map Y y$

--" ------------------
--" copy and paste with "+y and "+p

--vmap <C-c> "+yi
--vmap <C-x> "+c
--vmap <C-v> c<ESC>"+p
--imap <C-v> <ESC>"+pa

--"vnoremap <c-c>"+y
--"map <c-v>"+P
--" ------------------</c-v></c-c>
--
--cmap w!! %!sudo tee > /dev/null %

--cmap w! !sudo tee %




--[[
local Plug = vim.fn['plug#']

vim.call('plug#begin', '~/.config/nvim/autoload')

local nvimrc = os.getenv("NVIMRC") -- get nvimrc path vim.cmd('source' ~/.config/nvim/plugins/abolish.vim)
source ~/.config/nvim/plugins/arduino.vim
source ~/.config/nvim/plugins/airline.vim
source ~/.config/nvim/plugins/bufferline.vim
source ~/.config/nvim/plugins/coc.vim
  "source ~/.config/nvim/plugins/clearreg1.vim
  "source ~/.config/nvim/plugins/clearreg.vim
  "luafile ~/.config/nvim/lua/setup/plug-colorizer.lua
source ~/.config/nvim/plugins/colorizer.vim
source ~/.config/nvim/plugins/coc-explorer.vim
source ~/.config/nvim/plugins/commentary.vim
source ~/.config/nvim/plugins/vim-airline-themes.vim
source ~/.config/nvim/plugins/context-commentstring.vim
source ~/.config/nvim/plugins/dispatch.vim
source ~/.config/nvim/plugins/dracula.vim
source ~/.config/nvim/plugins/editorconfig.vim
source ~/.config/nvim/plugins/eunuch.vim
source ~/.config/nvim/plugins/exchange.vim
source ~/.config/nvim/plugins/firenvim.vim
source ~/.config/nvim/plugins/floaterm.vim
source ~/.config/nvim/plugins/fugitive.vim
source ~/.config/nvim/plugins/fvim.vim
source ~/.config/nvim/plugins/fzf.vim
source ~/.config/nvim/plugins/gitsigns.nvim.vim
source ~/.config/nvim/plugins/git-worktree.vim
source ~/.config/nvim/plugins/heritage.vim
source ~/.config/nvim/plugins/lastplace.vim
source ~/.config/nvim/plugins/lion.vim
#luafile ~/.config/nvim/lua/setup/lsp-installer.lua
source ~/.config/nvim/plugins/lspkind-nvim.vim
source ~/.config/nvim/plugins/markdown-preview.vim
source ~/.config/nvim/plugins/neogen.vim
source ~/.config/nvim/plugins/nerdtree.vim
source ~/.config/nvim/plugins/nvim-cmp.vim
#source ~/.config/nvim/plugins/nvim-lspconfig.vim
source ~/.config/nvim/plugins/nvim-lsp-installer.vim
source ~/.config/nvim/plugins/nvim-notify.vim
source ~/.config/nvim/plugins/nvim-web-devicon.vim
source ~/.config/nvim/plugins/pasta.vim
source ~/.config/nvim/plugins/peekaboo.vim
source ~/.config/nvim/plugins/phpactor.vim
source ~/.config/nvim/plugins/polyglot.vim
source ~/.config/nvim/plugins/projectionist.vim
source ~/.config/nvim/plugins/quickscope.vim
source ~/.config/nvim/plugins/repeat.vim
source ~/.config/nvim/plugins/rnvimr.vim
source ~/.config/nvim/plugins/rooter.vim
source ~/.config/nvim/plugins/rust.vim
source ~/.config/nvim/plugins/sayonara.vim
source ~/.config/nvim/plugins/smooth-scroll.vim
source ~/.config/nvim/plugins/splitjoin.vim
source ~/.config/nvim/plugins/sudoedit.vim
source ~/.config/nvim/plugins/surround.vim
source ~/.config/nvim/plugins/targets.vim
source ~/.config/nvim/plugins/telescope.nvim.vim
source ~/.config/nvim/plugins/textobj-xmlattr.vim
source ~/.config/nvim/plugins/tmux-navigator.vim
source ~/.config/nvim/plugins/nvim-tree.lua
source ~/.config/nvim/plugins/nvim-treesitter.vim
source ~/.config/nvim/plugins/unimpaired.vim
source ~/.config/nvim/plugins/vim-livedown.vim
source ~/.config/nvim/plugins/vim-markdown.vim
source ~/.config/nvim/plugins/vim-magit.vim
source ~/.config/nvim/plugins/vim-test.vim
source ~/.config/nvim/plugins/vim-textobj-between.vim
source ~/.config/nvim/plugins/vim-textobj-brace.vim
source ~/.config/nvim/plugins/vim-textobj-comment.vim
source ~/.config/nvim/plugins/vim-textobj-continuous-line.vim
source ~/.config/nvim/plugins/vim-textobj-datetime.vim
source ~/.config/nvim/plugins/vim-textobj-diff.vim
source ~/.config/nvim/plugins/vim-textobj-entire.vim
source ~/.config/nvim/plugins/vim-textobj-erb.vim
source ~/.config/nvim/plugins/vim-textobj-fold.vim
source ~/.config/nvim/plugins/vim-textobj-user.vim
source ~/.config/nvim/plugins/visual-multi.vim
source ~/.config/nvim/plugins/visual-star-search.vim
"source ~/.config/nvim/plugins/which-key-folke.vim
source ~/.config/nvim/plugins/which-key.vim
source ~/.config/nvim/plugins/wordwrap.vim


vim.call('plug#end')
]
