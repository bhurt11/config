Plug 'neovim/nvim-lspconfig'
Plug 'williamboman/nvim-lsp-installer'

    config = function()
      local capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())
      local on_attach = require'config.on_attach'
      require('nvim-lsp-installer').on_server_ready(
        function(server)
          local config = {
            on_attach = on_attach[server.name],
            capabilities = capabilities,
            autostart = true,
            settings = {
              Lua = {
                diagnostics = { globals = {'vim'} }
              }
            }
          }
          server:setup(config)
        end
      )
    end
  }

