set runtimepath^=~/.vim runtimepath+=~/.vim/after
    let &packpath = &runtimepath
    source ~/.vimrc
doautocmd User PlugLoaded
let g:airline_theme='distinguished'

source ~/.config/nvim/lua/bufferline/bufferline.lua

lua require'colorizer'.setup()

lua require("bufferline").setup{}

lua require('bufferline')

set termguicolors

lua require'lspconfig'.pyright.setup{}
